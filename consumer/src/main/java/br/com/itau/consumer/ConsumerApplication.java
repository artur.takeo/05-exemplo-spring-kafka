package br.com.itau.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.converter.RecordMessageConverter;
import org.springframework.kafka.support.converter.StringJsonMessageConverter;
import org.springframework.messaging.handler.annotation.Payload;

@SpringBootApplication
public class ConsumerApplication {

	Logger logger = LoggerFactory.getLogger(ConsumerApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(ConsumerApplication.class, args);
	}
	
	@Bean
	public RecordMessageConverter converter() {
		return new StringJsonMessageConverter();
	}
	
	@KafkaListener(id="consumer", topics="hottopic")
	public void consumir(@Payload Message message) {
		System.out.println(message.getContent());
	}
}
