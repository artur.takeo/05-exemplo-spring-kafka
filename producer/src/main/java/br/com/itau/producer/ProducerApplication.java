package br.com.itau.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class ProducerApplication {
	
	@Autowired
	KafkaTemplate<Object, Object> kafkaTemplate;

	@PostMapping("/{what}")
	public void publicar(@PathVariable String what) {
		kafkaTemplate.send("hottopic", new Message(what));
	}
	
	public static void main(String[] args) {
		SpringApplication.run(ProducerApplication.class, args);
	}
	
	class Message {
		private String content;
		
		public Message() {}
		
		public Message(String content) {
			this.content = content;
		}
		
		public void setContent(String content) {
			this.content = content;
		}
		
		public String getContent() {
			return content;
		}
	}

}
